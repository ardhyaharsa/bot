let mix = require('laravel-mix');

mix.js([
    'resources/javascripts/app.js',
    'resources/javascripts/facebook.js'
], 'public/javascripts/');
mix.sass('resources/stylesheets/app.scss', 'public/stylesheets/');

if (mix.inProduction()) {
    mix.version();
}