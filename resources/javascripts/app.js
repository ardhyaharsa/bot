import Vue from 'vue';
import router from './router';
import App from './views/App.vue';
import VueProgressBar from 'vue-progressbar';

Vue.use(VueProgressBar, {
    color: 'rgb(32, 156, 238)',
    failedColor: 'rgb(255, 56, 96)',
    height: '2px'
});

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');