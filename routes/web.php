<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'webhook'], function () use ($router) {
    $router->get('/messenger', 'WebhookController@validation');
    $router->post('/messenger', 'WebhookController@action');
    $router->get('/welcome', 'WebhookController@welcome');
});

$router->group(['prefix' => 'cron'], function () use ($router) {
    $router->get('/news', 'CronController@news');
    $router->get('/broadcast', 'CronController@broadcast');
    $router->get('/unsubscribe', 'CronController@unsubscribe');
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/robot', 'ApiController@robots');
});

$router->get('/{route:.*}/', 'MainController@index');