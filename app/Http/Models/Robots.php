<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Robots extends Model {
    protected $table = 'robots';
    public $timestamps = FALSE;
    protected $hidden = ['id'];
}