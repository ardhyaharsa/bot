<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UsersTags extends Model {
    protected $table = 'users_tags';
    public $timestamps = FALSE;
    
    function tags () {
        return $this->hasOne('App\Http\Models\Tags', 'id', 'tags_id');
    }
    
    function users () {
        return $this->hasOne('App\Http\Models\Users', 'id', 'users_id');
    }
}