<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class NewsSubs extends Model {
    protected $table = 'news_subs';
    public $timestamps = FALSE;
}