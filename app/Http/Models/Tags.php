<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model {
    protected $table = 'tags';
    public $timestamps = FALSE;
}