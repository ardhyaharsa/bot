<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model {
    protected $table = 'news';
    public $timestamps = FALSE;
    
    function subs () {
        return $this->hasMany('App\Http\Models\NewsSubs', 'news_id', 'id');
    }
}