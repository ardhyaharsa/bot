<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Core;
use Laravel\Lumen\Routing\Controller as BaseController;

class MainController extends BaseController
{
    public function index () {
        $manifest_json = file_get_contents(base_path('public/mix-manifest.json'));
        $data = [
            'manifest' => json_decode($manifest_json, TRUE)
        ];

        return view('app' , $data);
    }
}