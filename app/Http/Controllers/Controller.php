<?php

namespace App\Http\Controllers;

use App\Http\Models\Robots;
use GuzzleHttp\Client;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function send ($key_url, $params) {
        $client = new Client([
            'timeout' => 15,
            'verify' => FALSE
        ]);
        
        return $client->request('POST' , 'https://graph.facebook.com/v2.6/me/'.$key_url , [
            'query' => [
                'access_token' => env('MESSENGER_ACCESS_TOKEN')
            ],
            'json' => $params,
        ]);
    }
    
    public function robot ($key , $data) {
        Robots::where('name' , $key)->update($data);
    }
}