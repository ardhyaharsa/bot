<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Core;
use App\Http\Models\News;
use App\Http\Models\Users;
use App\Http\Models\UsersTags;
use App\Http\Models\Tags;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Log;

class WebhookController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function validation (Request $request) {
        $verify_token = $request->input('hub_verify_token');
        if ($verify_token == env('MESSENGER_VERIFY_TOKEN')) {
            return $request->input('hub_challenge');
        } else {
            return 'Invalid Verify Token';
        }
    }

    public function action (Request $request) {
        $response = [];
        $data = $request->all();
        
        $sender = Core::val($data , 'entry.0.messaging.0.sender');
        
        $postback = Core::val($data , 'entry.0.messaging.0.postback');
        $message = Core::val($data , 'entry.0.messaging.0.message');
        
        $type = '-';
        if ($postback) $type = 'postback';
        if ($message) $type = 'message';
        
        if ($type != '-') {
            $response = $this->_sender_action($sender, 'mark_seen');
            $sender = $this->_sender($sender);
            
            switch ($type) {
                case 'postback':
                    $payload = explode('_', Core::val($postback , 'payload'));
                    switch (Core::val($payload , '0')) {
                        case 'welcome':
                            $response = $this->_sender_action($sender, 'typing_on');
                            
                            $params = [
                                'recipient' => [
                                    'id' => Core::val($sender , 'id')
                                ],
                                'message' => [
                                    'text' => "Hai ".Core::val($sender , 'first_name')." \xF0\x9F\x91\x8B , ketikkan topik seputar Bola yang ingin kamu ikuti ."
                                ]
                            ];
                            $response = $this->send('messages', $params);
                            
                            break;
                            
                        case 'similar':
                            $response = $this->_sender_action($sender, 'typing_on');
                            
                            $name = Core::val($postback , 'title');
                            $response = $this->_tags($sender, $name);
                            
                            break;
                            
                        case 'unfollow':
                            $tags_id = Core::val($payload , '1');
                            $follow_tags_id = Core::val($payload , '2' , 0);
                            
                            $where = [
                                'users_id' => Core::val($sender , 'user_id'),
                                'tags_id' => $follow_tags_id
                            ];
                            $follow_tags = UsersTags::where($where)->first();

                            if ($follow_tags) {
                                $tags = Tags::where('id' , $follow_tags_id)->first();
                                $params = [
                                    'recipient' => [
                                        'id' => Core::val($sender , 'id')
                                    ],
                                    'message' => [
                                        'text' => "`".Core::val($tags , 'name')."` sudah terdaftar dalam daftar langganan ".Core::val($sender , 'first_name')." \xF0\x9F\x91\x8D ."
                                    ]
                                ];
                                $response = $this->send('messages', $params);
                            } else {
                                $tags = Tags::where('id' , $tags_id)->first();
                                if ($tags) {
                                    $response = $this->_sender_action($sender, 'typing_on');
                                
                                    $where = [
                                        'users_id' => Core::val($sender , 'user_id'),
                                        'tags_id' => $tags_id
                                    ];
                                    UsersTags::where($where)->delete();
                                    
                                    $params = [
                                        'recipient' => [
                                            'id' => Core::val($sender , 'id')
                                        ],
                                        'message' => [
                                            'text' => "`".Core::val($tags , 'name')."` berhasil dihapus \xF0\x9F\x98\xAD . Kamu tidak mendapat informasi mengenai topik ini lagi ."
                                        ]
                                    ];
                                    $response = $this->send('messages', $params);
                                    
                                    $tags = Tags::where('id' , $follow_tags_id)->first();
                                    if ($tags) {
                                        $where = [
                                            'users_id' => Core::val($sender , 'user_id'),
                                            'tags_id' => Core::val($tags , 'id')
                                        ];
                                        
                                        $params = $this->_insert_tags($sender , $where , Core::val($tags , 'name'));
                                        $response = $this->send('messages', $params);
                                    }
                                }
                            }
                            
                            break;
                            
                        case 'news':
                            $news_id = Core::val($payload , '1');
                            $subs_id = Core::val($payload , '2' , 1);
                            $news = News::with(['subs'])->where('id' , $news_id)->first();
                            
                            if ($news) {
                                $response = $this->_sender_action($sender, 'typing_on');
                                
                                $subs_total = count(Core::val($news , 'subs' , []));
                                foreach (Core::val($news , 'subs' , []) as $k=>$v) {
                                    $next = $k + 1;
                                    
                                    if ($subs_id == $k) {
                                        $content = json_decode($v['content'] , TRUE);
                                         
                                        $message = '';
                                        $messages = [];
                                        foreach ($content as $k2=>$v2) {
                                            if(filter_var($v2, FILTER_VALIDATE_URL) === FALSE) {
                                                if (strlen($message) + strlen($v2) <= env('MESSENGER_LENGTH_LIMIT')) {
                                                    $message .= $v2."\n"; 
                                                } else {
                                                    $messages[] = [
                                                        'type' => 'text',
                                                        'data' => $message
                                                    ];
                                                    $message = $v2;
                                                }
                                            } else {
                                                if (!empty($message)) {
                                                    $messages[] = [
                                                        'type' => 'text',
                                                        'data' => $message
                                                    ];
                                                    $message = '';
                                                }
                                                
                                                $messages[] = [
                                                    'type' => 'image',
                                                    'data' => $v2
                                                ];
                                            }
                                        }
                                        
                                        foreach ($messages as $k2=>$v2) {
                                            $message_send = [];
                                            switch($v2['type']) {
                                                case 'text':
                                                    $message_send = [
                                                        'text' => $v2['data']
                                                    ];
                                                    break;
                                                    
                                                case 'image':
                                                    $message_send = [
                                                        'attachment' => [
                                                            'type' => 'image',
                                                            'payload' => [
                                                                'url' => $v2['data'],
                                                                'is_reusable' => TRUE
                                                            ]
                                                        ]
                                                    ];
                                                    break;
                                            }
                                            
                                            $params = [
                                                'recipient' => [
                                                    'id' => Core::val($sender , 'id')
                                                ],
                                                'message' => $message_send
                                            ]; 
                                            $response = $this->send('messages', $params);
                                        }
                                        
                                        if ($subs_total > $next) {
                                            $params = [
                                                'recipient' => [
                                                    'id' => Core::val($sender , 'id')
                                                ],
                                                'message' => [
                                                    'attachment' => [
                                                        'type' => 'template',
                                                        'payload' => [
                                                            'template_type' => 'button',
                                                            'text' => $message,
                                                            'buttons' => [
                                                                [
                                                                    'type' => 'postback',
                                                                    'title' => 'Lanjutkan',
                                                                    'payload' => 'news_'.$v['news_id'].'_'.$next,
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ];
                                            
                                            $response = $this->send('messages', $params);
                                        } else if (!empty($message)) {
                                            $params = [
                                                'recipient' => [
                                                    'id' => Core::val($sender , 'id')
                                                ],
                                                'message' => [
                                                    'text' => $message
                                                ]
                                            ];
                                            
                                            $response = $this->send('messages', $params);
                                        }
                                    }   
                                }
                            }
                            
                            break;
                        
                        case 'subscribe':
                            $response = $this->_sender_action($sender, 'typing_on');
                            
                            $params = [
                                'recipient' => [
                                    'id' => Core::val($sender , 'id')
                                ],
                                'message' => [
                                    'text' => "Hai ".Core::val($sender , 'first_name')." \xF0\x9F\x91\x8B , terimakasih sudah berlangganan kembali ."
                                ]
                            ];
                            $response = $this->send('messages', $params);
                            
                            break;
                    }
                    
                    break;
                
                case 'message':
                    $response = $this->_sender_action($sender, 'typing_on');
                    
                    $text = trim(Core::val($message , 'text'));
                    $response = $this->_tags($sender, $text);
                    if (!$response) {
                        $stemmer_factory = new \Sastrawi\Stemmer\StemmerFactory();
                        $stemmer = $stemmer_factory->createStemmer();
                        
                        $score = [];
                        $tags = Tags::get();
                        foreach ($tags as $k=>$v) {
                            similar_text($stemmer->stem($text), $v['name_stem'], $similar);
                            $score[$similar] = $v;
                        }
                        krsort($score);
                        $score = array_values($score);
                        $score = array_slice($score, 0, 3);
                        
                        $buttons = [];
                        foreach ($score as $k=>$v) {
                            $buttons[] = [
                                'type' => 'postback',
                                'title' => $v['name'],
                                'payload' => 'similar',
                            ];
                        }
                        
                        $params = [
                            'recipient' => [
                                'id' => Core::val($sender , 'id')
                            ],
                            'message' => [
                                'attachment' => [
                                    'type' => 'template',
                                    'payload' => [
                                        'template_type' => 'button',
                                        'text' => "Maaf \xF0\x9F\x99\x87 ".Core::val($sender , 'first_name')." , kami belum bisa memberikan berita sesuai keinginanmu. Mungkin kamu tertarik dengan alternatif berita berikut :",
                                        'buttons' => $buttons
                                    ]
                                ]
                            ]
                        ];
                        $response = $this->send('messages', $params);
                    }
                    
                    break;
            }
        }
        
        return response()->json($response);
    }
    
    public function welcome () {
        $params = [
            'get_started' => [
                'payload' => 'welcome'
            ],
            'greeting' => [
                [
                    'locale' => 'default',
                    'text' => "Hai {{user_first_name}} , senang bisa berjumpa denganmu \xF0\x9F\x91\x8B . Klik link dibawah \xF0\x9F\x91\x87 untuk memulai !"
                ]
            ]
        ];
        $response = $this->send('messenger_profile', $params);
        
        return response()->json($response);
    }
    
    private function _sender_action ($sender, $type) {
        $params = [
            'recipient' => [
                'id' => Core::val($sender , 'id')
            ],
            'sender_action' => $type
        ];
        return $this->send('messages', $params);
    }
    
    private function _sender ($sender) {
        $client = new Client([
            'timeout' => 15,
            'verify' => FALSE
        ]);
        
        $response = $client->request('GET' , 'https://graph.facebook.com/'.Core::val($sender , 'id') , [
            'query' => [
                'fields' => 'name,first_name,last_name,profile_pic',
                'access_token' => env('MESSENGER_ACCESS_TOKEN')
            ]
        ]);
        
        $user = json_decode($response->getBody(), TRUE);
        $user['last_update'] = date('Y-m-d H:i:s');
        $user['is_active'] = TRUE;
        unset($user['id']);
        
        $user_id = 0;
        $where = [
            'social_id' => Core::val($sender , 'id'),
            'social_type' => 'messenger'
        ];
        $users = Users::where($where)->first();
        if (!$users) {
            $user['social_id'] = Core::val($sender , 'id');
            $user['social_type'] = 'messenger';
            $user['create_date'] = date('Y-m-d H:i:s');
            
            $user_id = Users::insertGetId($user);
            unset($user['social_id'] , $user['social_type'] , $user['create_date']);
        } else {
            Users::where($where)->update($user);
            
            $user_id = Core::val($users , 'id');
        }
        
        $user['id'] = Core::val($sender , 'id');
        $user['user_id'] = $user_id;
                
        return $user;
    }
    
    private function _tags ($sender, $name) {
        $limit = 3;
        $tags = Tags::where('name' , $name)->first();
        
        if ($tags) {
            $where = [
                'users_id' => Core::val($sender , 'user_id'),
            ];
            $user_tags = UsersTags::with(['tags'])->where($where)->get();
            if (count($user_tags) < $limit) {
                $where = [
                    'tags_id' => Core::val($tags , 'id'),
                ];
                $params = $this->_insert_tags($sender , $where , Core::val($tags , 'name'));
            } else {
                $buttons = [];
                foreach ($user_tags as $k=>$v) {
                    $buttons[] = [
                        'type' => 'postback',
                        'title' => Core::val($v , 'tags.name'),
                        'payload' => 'unfollow_'.Core::val($v , 'tags_id').'_'.Core::val($tags , 'id'),
                    ];
                }
                
                $params = [
                    'recipient' => [
                        'id' => Core::val($sender , 'id')
                    ],
                    'message' => [
                        'attachment' => [
                            'type' => 'template',
                            'payload' => [
                                'template_type' => 'button',
                                'text' => "Maaf \xF0\x9F\x99\x87 ".Core::val($sender , 'first_name')." , jumlah maksimal berlangganan adalah ".$limit." topik . Pilih topik untuk dihapus :",
                                'buttons' => $buttons
                            ]
                        ]
                    ]
                ];
            }
            
            return $this->send('messages', $params);                                                                
        } else {
            return FALSE;
        }
    }
    
    private function _insert_tags($sender , $where , $name) {
        $user_tags = UsersTags::where($where)->first();
        if (!$user_tags) {
            UsersTags::insert($where);
        }
        
        return [
            'recipient' => [
                'id' => Core::val($sender , 'id')
            ],
            'message' => [
                'text' => "`".$name."` ".($user_tags ? "sudah terdaftar" : "berhasil ditambahkan")." dalam daftar langganan ".Core::val($sender , 'first_name')." \xF0\x9F\x91\x8D ."
            ]
        ];
    }
}
