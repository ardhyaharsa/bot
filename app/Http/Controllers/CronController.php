<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Core;
use App\Http\Models\News;
use App\Http\Models\NewsSubs;
use App\Http\Models\Tags;
use App\Http\Models\Users;
use App\Http\Models\UsersTags;
use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Log;
use Symfony\Component\DomCrawler\Crawler;

class CronController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function news() {
        $robot = [
            'status' => FALSE,
            'start' => date('Y-m-d H:i:s'),
            'end' => NULL,
        ];
        $this->robot('news' , $robot);
        
        $stemmer_factory = new \Sastrawi\Stemmer\StemmerFactory();
        $stemmer = $stemmer_factory->createStemmer();
        
        $results = [];

        $client = new Client([
            'timeout' => 15,
            'verify' => FALSE
        ]);
        
        $response = $client->get('https://sindikasi.okezone.com/index.php/rss/14/RSS2.0');
        $xml = simplexml_load_string($response->getBody());
        $rss = Core::toArray($xml);
        
        foreach (Core::val($rss , 'channel.item' , []) as $k=>$v) {
            $url = parse_url($v['link']);
            $path = $url['path'];
            
            $news = News::where('url' , $path)->first();
            if (!$news && count($results) < 25) {
                $results[] = [
                    'pub_date' => Core::dateFormat($v['pubDate'] , 'Y-m-d H:i:s' , DATE_RFC2822),
                    'path' => $path
                ];
            }
        }
        
        foreach ($results as $k=>$v) {
            $contents = [];
            
            $next = 1;
            $looping = TRUE;
            while ($looping) {
                $fetch = $this->_fetch(Core::val($v , 'path') , $next);
                
                if(count(Core::val($fetch , 'contents' , [])) == 0 || Core::val($fetch , 'paging' , 0) == 0) {
                    if(Core::val($fetch , 'paging' , 0) == 0) $contents[] = $fetch;
                    $looping = FALSE;
                } else {
                    $contents[] = $fetch;
                }
                
                $next++;
            }
            
            if(!empty($contents)) {
                if(count($contents) > 1) {
                    foreach($contents as $k2=>$v2) {
                        if($k2 > 0) {
                            $contents[0]['contents'] = array_diff($contents[0]['contents'], Core::val($v2 , 'contents'));
                        }
                    }
                }
                
                $tags = [];
                foreach(Core::val($contents , '0.tags' , []) as $k2=>$v2) {
                    $tag = Tags::where('name' , $v2)->first();
                    if ($tag) {
                        $tags[] = Core::val($tag , 'id');
                    } else {
                        $tag = [
                            'name' => $v2,
                            'name_stem' => $stemmer->stem(strtolower(preg_replace("/[^A-Za-z0-9 ]/", '', $v2)))
                        ];
                        $tags[] = Tags::insertGetId($tag);
                    }
                }
                
                $image = Core::val($contents , '0.image');
                $response_code = $this->_exists($image);
                if ($response_code == 200) {
                    $news = [
                        'url' => Core::val($v , 'path'),
                        'title' => Core::val($contents , '0.title'),
                        'image' => Core::val($contents , '0.image'),
                        'tags' => json_encode(array_unique($tags)),
                        'create_date' => Core::val($v , 'pub_date'),
                        'broadcast' => 0
                    ];
                    $news_id = News::insertGetId($news);
                    
                    foreach($contents as $k2=>$v2) {
                        $news_subs = [
                            'news_id' => $news_id,
                            'content' => json_encode(Core::val($v2 , 'contents')),
                            'orders' => $k2
                        ];
                        NewsSubs::insert($news_subs);
                    } 
                } else {
                    $results[$k]['code'] = $response_code;
                }
            } else {
                Log::info($v);
            }
        }
        
        $robot = [
            'status' => TRUE,
            'end' => date('Y-m-d H:i:s'),
        ];
        $this->robot('news' , $robot);

        return response()->json($results);
    }
    
    public function broadcast() {
        $robot = [
            'status' => FALSE,
            'start' => date('Y-m-d H:i:s'),
            'end' => NULL,
        ];
        $this->robot('broadcast' , $robot);
        
        $news = News::with(['subs'])
                        ->where('broadcast' , 0)
                        ->orderBy('create_date' , 'ASC')
                        ->limit(env('MESSENGER_BROADCAST_LIMIT'))                        
                        ->get();
        foreach ($news as $k=>$v) {
            $tags_arr = $users_tags_arr = [];
            
            $tags = json_decode($v['tags'] , TRUE);
            $tags_ = Tags::select('id','name')->whereIn('id' , $tags)->get();
            foreach ($tags_ as $k2=>$v2) {
                $tags_arr[$v2['id']] = $v2;
            }
            
            $filter = UsersTags::whereIn('tags_id', $tags)
                                    ->with(['users'])
                                    ->whereHas('users', function ($query) {
                                        $query->where('is_active' , TRUE);
                                    });
            
            $users = clone $filter;
            $users = $users->select('users_id')->groupBy('users_id')->get();
            if (!empty($users)) {
                $content = [
                    $v['title'],
                    ''
                ];
                
                $content = array_merge($content , json_decode(Core::val($v , 'subs.0.content' , '[]'), TRUE));
                
                $message = '';
                $messages = [];
                foreach ($content as $k2=>$v2) {
                    if(filter_var($v2, FILTER_VALIDATE_URL) === FALSE) {
                        if (strlen($message) + strlen($v2) <= env('MESSENGER_LENGTH_LIMIT')) {
                            $message .= $v2."\n"; 
                        } else {
                            $messages[] = [
                                'type' => 'text',
                                'data' => $message
                            ];
                            $message = $v2;
                        }
                    } else {
                        if (!empty($message)) {
                            $messages[] = [
                                'type' => 'text',
                                'data' => $message
                            ];
                            $message = '';
                        }
                        
                        $messages[] = [
                            'type' => 'image',
                            'data' => $v2
                        ];
                    }
                }
                
                $limit = count(Core::val($v , 'subs' , [])) == 1 ? 3 : 2;
                
                $users_tags = clone $filter;
                $users_tags = $users_tags->select('users_id','tags_id')->groupBy('users_id','tags_id')->get();
                foreach ($users_tags as $k2=>$v2) {
                    if (!isset($users_tags_arr[$v2['users_id']])) $users_tags_arr[$v2['users_id']] = [];
                    if (count($users_tags_arr[$v2['users_id']]) < $limit) $users_tags_arr[$v2['users_id']][] = $v2['tags_id'];
                }
                
                foreach ($users as $k2=>$v2) {
                    $params = [
                        'recipient' => [
                            'id' => Core::val($v2 , 'users.social_id')
                        ],
                        'message' => [
                            'attachment' => [
                                'type' => 'image',
                                'payload' => [
                                    'url' => $v['image'],
                                    'is_reusable' => TRUE
                                ]
                            ]
                        ]
                    ];
                    $response = $this->send('messages', $params);
                    
                    foreach ($messages as $k3=>$v3) {
                        $message_send = [];
                        switch($v3['type']) {
                            case 'text':
                                $message_send = [
                                    'text' => $v3['data']
                                ];
                                break;
                                
                            case 'image':
                                $message_send = [
                                    'attachment' => [
                                        'type' => 'image',
                                        'payload' => [
                                            'url' => $v3['data'],
                                            'is_reusable' => TRUE
                                        ]
                                    ]
                                ];
                                break;
                        }
                        
                        $params = [
                            'recipient' => [
                                'id' => Core::val($v2 , 'users.social_id')
                            ],
                            'message' => $message_send
                        ];
                        $response = $this->send('messages', $params);
                    }
                    
                    $buttons = [];
                    foreach ($users_tags_arr[$v2['users_id']] as $k3=>$v3) {
                        $buttons[] = [
                            'type' => 'postback',
                            'title' => 'Unfollow `'.Core::val($tags_arr , $v3.'.name').'`',
                            'payload' => 'unfollow_'.$v3,
                        ];
                    }
                    
                    if(count(Core::val($v , 'subs' , [])) > 1) {
                        $buttons[] = [
                            'type' => 'postback',
                            'title' => 'Lanjutkan',
                            'payload' => 'news_'.$v['id'],
                        ];
                    }
                    
                    $params = [
                        'recipient' => [
                            'id' => Core::val($v2 , 'users.social_id')
                        ],
                        'message' => [
                            'attachment' => [
                                'type' => 'template',
                                'payload' => [
                                    'template_type' => 'button',
                                    'text' => !empty($message) ? $message : "\xE2\x9A\xAA",
                                    'buttons' => $buttons
                                ]
                            ]
                        ]
                    ];
                    $response = $this->send('messages', $params);
                }
            }
            
            $update = [
                'broadcast' => 1
            ];
            News::where('id' , $v['id'])->update($update);
        }
        
        $robot = [
            'status' => TRUE,
            'end' => date('Y-m-d H:i:s'),
        ];
        $this->robot('broadcast' , $robot);
        
        return response()->json([]);             
    }
    
    public function unsubscribe () {
        $robot = [
            'status' => FALSE,
            'start' => date('Y-m-d H:i:s'),
            'end' => NULL,
        ];
        $this->robot('unsubscribe' , $robot);
        
        $date = new Datetime();
        $date->modify('-1 day');
        
        $user = Users::where('is_active' , TRUE)
                        ->where('last_update' , '<' , $date->format('Y-m-d H:i:s'))
                        ->get();
        
        foreach($user as $k=>$v) {
            $message = "Hi ".Core::val($v , 'first_name')."\n\n".
                        "Sudah 1 hari berlalu sejak terakhir kali kamu membalas pesanku \xF0\x9F\x98\xA2 . ".
                        "Facebook akan otomatis menghapus datamu jika tidak ada interaksi dari kamu dalam jangka waktu 1 hari . ".
                        "Apabila kamu masih tertarik untuk berlangganan , klik tombol `Langganan` dibawah .\n".
                        "Terimakasih \xF0\x9F\x99\x8F";
            
            $params = [
                'recipient' => [
                    'id' => Core::val($v , 'social_id')
                ],
                'message' => [
                    'attachment' => [
                        'type' => 'template',
                        'payload' => [
                            'template_type' => 'button',
                            'text' => $message,
                            'buttons' => [
                                [
                                    'type' => 'postback',
                                    'title' => 'Langganan',
                                    'payload' => 'subscribe',
                                ]
                            ]
                        ]
                    ]
                ]
            ];
            $response = $this->send('messages', $params);
            
            $update = [
                'is_active' => FALSE
            ];
            Users::where('id' , Core::val($v , 'id'))->update($update);
        }
        
        $robot = [
            'status' => TRUE,
            'end' => date('Y-m-d H:i:s'),
        ];
        $this->robot('unsubscribe' , $robot);
        
        return response()->json([]);
    }
    
    private function _fetch ($path , $page) {
        $client = new Client([
            'base_uri' => 'https://bola.okezone.com',
            'timeout' => 15,
            'verify' => FALSE
        ]);
        
        $response = $client->get($path , [
            'query' => [
                'page' => $page
            ]
        ]);
        $stream = $response->getBody();
        $crawler = new Crawler($stream->getContents());
        
        $masters = [
            [
                'content' => '.detail .read',
                'tag' => '.detail-tag.newtag li'
            ]
        ];
        
        $contents = $tags = [];
        foreach ($masters as $master) {
            $content = $crawler->filter($master['content']);
            foreach ($content as $k2=>$v2) {
                $crawler2 = new Crawler($v2);
                
                $p = $crawler2->filter('p');
                foreach ($p as $k3=>$v3) {
                    $crawler3 = new Crawler($v3);
                    
                    $text = trim($crawler3->text());
                    $insert = TRUE;
                    $skip_contains = [
                        'baca juga'
                    ];
                    foreach ($skip_contains as $k=>$v) {
                        if(strpos(strtolower($text) , $v) !== FALSE) $insert = FALSE;
                    }
                    if (!empty($text) && $insert) $contents[] = $text;
                    
                    $image = $crawler3->filter('img');
                    $count = $crawler3->filter('img')->count();
                    if ($image->count() > 0) {
                        foreach ($image as $k4=>$v4) {
                            $crawler4 = new Crawler($v4);
                            $image_source = $crawler4->attr('src');
                            $response_code = $this->_exists($image_source);
                            if ($response_code == 200) {
                                $contents[] = $image_source;
                            }
                        }
                    }
                }
            }
            
            $list_tag = $crawler->filter($master['tag']);
            foreach ($list_tag as $k2=>$v2) {
                $crawler2 = new Crawler($v2);
                $tags[] = ucwords(str_replace('#' , '' , trim($crawler2->text())));
            }
        }
        
        return [
            'path' => $path,
            'page' => $page,
            'title' => $crawler->filter('h1')->text(),
            'image' => $crawler->filterXpath('//meta[@property="og:image"]')->attr('content'),
            'contents' => $contents,
            'tags' => $tags,
            'paging' => $crawler->filter('.paging')->count()
        ];
    }
    
    private function _exists ($url) {
        $code = 405;
        
        $client = new Client([
            'timeout' => 15,
            'verify' => FALSE
        ]);
        
        try {
            $res = $client->request('GET', $url);
            $code = $res->getStatusCode();
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $code = $e->getResponse()->getStatusCode();
            }
        }
        
        return $code;
    }
}
