<?php

namespace App\Http\Controllers;

use App\Http\Models\Robots;
use DateTime;

class ApiController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function robots() {
        $robot = Robots::get();
        foreach ($robot as $k=>$v) {
            $v['url'] = url($v['url']);
            
            $start = new DateTime($v['start']);
            $v['start'] = $start->format('d F Y H:i');
            
            $date_diff = 0;
            if ($v['status']) {
                $end = new DateTime($v['end']);
                $date_diff = date_diff($start , $end)->s;
            }
            $v['date_diff'] = $date_diff;
            
            $robot[$k] = $v;
        }

        return response()->json($robot);
    }
}
