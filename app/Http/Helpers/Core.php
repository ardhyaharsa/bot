<?php

namespace App\Http\Helpers;

use DateTime;

class Core {
    public static function echoPre ($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
    
    public static function dateFormat ($date , $to = 'Y-m-d H:i:s' , $from = 'Y-m-d H:i:s') {
        $dateTime = DateTime::createFromFormat($from , $date);
        return $dateTime->format($to);
    }

    public static function toArray ($data) {
        return json_decode(json_encode($data), TRUE);
    }

    public static function val ($row , $key , $default = FALSE) {
        if (is_object($row)) $row = self::toArray($row);

        if (strpos($key , '.')) {
            $arr = explode('.' , $key);
            for ($i = 0 ; $i < count($arr) - 1 ; $i++) {
                $row = self::val($row , $arr[$i] , $default);
            }

            return self::val($row , end($arr) , $default);
        } else {
            return isset($row[$key]) && $row[$key] ? $row[$key] : $default;
        }
    }
}